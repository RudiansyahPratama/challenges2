import React from 'react';
import Avatar from './Avatar';
import UserName from './UserName';
import Bio from './Bio';

const UserProfile = () => (
    <div>
        <Avatar />
        <UserName username="rudiansyahpratama" />
        <Bio bio="ZERO" />
    </div>
   
);


export default UserProfile;