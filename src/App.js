import React from 'react';
import './App.css';
import Greetting from './Greetting';
import Clock from './Clock';
import UserProfile from './UserProfile/UserProfile';
//import UserProfile from './UserProfile/UserProfile';

function App() {
  return (
    <div className="App">
      <Greetting name="Rudi"/>
      <Greetting  age="19"/>
      <Greetting  gender="male"/>
      <UserProfile />
      <Clock />
    </div>
  );
}

export default App;
