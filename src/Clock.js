import React from 'react';

class Clock extends React.Component {
    constructor(props) {
        super(props);
        this.state = { date: new Date() };
    }
        
    componentDidMount() {
        this.timerID = setInterval(() => this.updateClock(), 1000);
    }

    componentWillUnmount() {
        clearInterval(this.timerID);
    }

    updateClock() {
        this.setState({
        date: new Date()
        });
    }

    render() {
        return (
            <div>
            <h1>Hello, world!</h1>
            <h2>Jam {this.state.date.toLocaleTimeString()}.</h2>
            </div>
        );
    }
}

export default Clock;